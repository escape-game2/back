export const subscribe = (mqtt, topic) =>
  mqtt.subscribe(topic, (err) => {
    if (err) console.log(`error subscribing to ${topic}`);
  });

export const publish = (mqtt, topic, message) =>
  mqtt.publish(topic, JSON.stringify(message));
