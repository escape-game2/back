import { MIN_PHOTO_RESISTOR_VALUE, ROUTERS } from '../constants.js';
import * as mqttController from './mqtt.controllers.js';
import xbee_api from 'xbee-api';
const { constants: C } = xbee_api;

import { isCorrectPressure } from '../utils.js';

export const zigbeeIoDataSampleRx = (mqtt, xbeeAPI, frame) => {
  const ni = ROUTERS[frame.remote64];
  const value = frame.analogSamples.AD0;
  console.log({ ni, value });

  switch (ni) {
    case 'pressure-sensor':
      checkPressure(xbeeAPI, mqtt, value, ni);
      break;
    case 'photoresistor-sensor':
      checkPhotoresistor(mqtt, value, ni);
      break;
    default:
      console.log('>> NI NOT HANDLED');
      console.log({ ni, value });
  }
};

const checkPressure = (xbeeAPI, mqtt, value, ni) => {
  if (isCorrectPressure(value)) {
    console.log('>> CORRECT PRESSURE');
    const topic = `/escape-game/rooms/1/${ni}`;
    mqttController.publish(mqtt, topic, 'you found the correct pressure');
    toggleStriker(xbeeAPI, 'open');
  }
};
const checkPhotoresistor = (mqtt, value, ni) => {
  if (value >= MIN_PHOTO_RESISTOR_VALUE) {
    console.log('>> CORRECT LUX');
    const topic = `/escape-game/rooms/1/${ni}`;
    mqttController.publish(mqtt, topic, 'you found the correct lux');
  }
};

export const toggleStriker = (xbeeAPI, action) => {
  const frame_obj = {
    type: C.FRAME_TYPE.REMOTE_AT_COMMAND_REQUEST,
    destination64: '0013A20041C34AB8',
    command: 'D0',
    commandParameter: [action === 'open' ? 5 : 4]
  };

  xbeeAPI.builder.write(frame_obj);
};
