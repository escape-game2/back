export const ROUTERS = {
  '0013a20041a7133c': 'pressure-sensor',
  '0013A20041C34AB8': 'magnetic-striker',
  '0013a20041fb76ea': 'photoresistor-sensor',
  '0013a20041FB7701': 'coordinator'
};

export const CORRECT_PRESSURE_VALUE = 480;
export const CORRECT_PRESSURE_RANGE = 80;

export const MIN_PHOTO_RESISTOR_VALUE = 100;
