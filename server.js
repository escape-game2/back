import SerialPort from 'serialport';
import xbee_api from 'xbee-api';
import dotenv from 'dotenv';
import mqtt from 'mqtt';
import {
  toggleStriker,
  zigbeeIoDataSampleRx
} from './controllers/xbee.controllers.js';
import { ROUTERS } from './constants.js';

const mqttClient = mqtt.connect('ws://broker.emqx.io:8083/mqtt');
const { XBeeAPI, constants: C } = xbee_api;
dotenv.config();

const SERIAL_PORT = process.env.SERIAL_PORT;

const xbeeAPI = new XBeeAPI({
  api_mode: 2
});

let serialport = new SerialPort(
  SERIAL_PORT,
  { baudRate: parseInt(process.env.SERIAL_BAUDRATE) || 9600 },
  (err) => {
    if (err) return console.log('Error: ', err.message);
  }
);

serialport.pipe(xbeeAPI.parser);
xbeeAPI.builder.pipe(serialport);

serialport.on('open', () => {
  console.log('serial port open');
  //toggleStriker(xbeeAPI, 'close');
});
mqttClient.on('connect', () => console.log('mqtt connected'));

xbeeAPI.parser.on('data', (frame) => {
  switch (frame.type) {
    case C.FRAME_TYPE.ZIGBEE_IO_DATA_SAMPLE_RX:
      console.log('>> ZIGBEE_IO_DATA_SAMPLE_RX');
      zigbeeIoDataSampleRx(mqttClient, xbeeAPI, frame);
      break;
    case C.FRAME_TYPE.REMOTE_COMMAND_RESPONSE:
      console.log('>> REMOTE_COMMAND_RESPONSE');
      const ni = ROUTERS[frame.remote64];
      console.log({ ni });
      break;
    default:
      console.log('>> FRAME TYPE NOT HANDLED');
      console.log(frame);
  }
});
