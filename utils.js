import { CORRECT_PRESSURE_VALUE, CORRECT_PRESSURE_RANGE } from './constants.js';

export const isCorrectPressure = (value) =>
  value >= CORRECT_PRESSURE_VALUE - CORRECT_PRESSURE_RANGE &&
  value <= CORRECT_PRESSURE_VALUE + CORRECT_PRESSURE_RANGE;
